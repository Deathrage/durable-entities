namespace Durable.TestCommon.Model
{
	public class Person
	{
		public int Id { get; set; }

		public string? Name { get; set; }

		public decimal Balance { get; set; }
	}
}
