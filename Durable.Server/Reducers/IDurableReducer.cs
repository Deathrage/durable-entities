﻿using Durable.Common.Actions;
using Durable.Server.ChangeTrackers;
using System.Threading.Tasks;

namespace Durable.Server.Reducers
{
	public interface IDurableReducer
	{
		ValueTask<object?> ReduceAsync(IDurableAction action, IDurableEntityChangeTracker changeTracker);
	}
}
