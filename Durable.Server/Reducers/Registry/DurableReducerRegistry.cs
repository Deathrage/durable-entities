﻿using Durable.Common.Actions;
using System;
using System.Collections.Concurrent;

namespace Durable.Server.Reducers.Registry
{
	public class DurableReducerRegistry : IDurableReducerRegistry
	{
		private readonly ConcurrentDictionary<Type, Func<IDurableReducer>> _actionReducers = new ConcurrentDictionary<Type, Func<IDurableReducer>>();

		public IDurableReducerRegistryBuilder Register<TAction>(Func<IDurableReducer> reducerAccessor)
			where TAction : IDurableAction
		{
			Type actionType = typeof(TAction);

			_actionReducers.AddOrUpdate(actionType,
				_ => reducerAccessor,
				(_, _) => reducerAccessor);

			return this;
		}

		public IDurableReducer ResolveReducer(Type actionType)
		{

			if (_actionReducers.TryGetValue(actionType, out Func<IDurableReducer>? reducerAccessor) && reducerAccessor is not null)
			{
				IDurableReducer reducer = reducerAccessor();

				if (reducer is null)
					throw new InvalidOperationException("Reducer cannot be null");

				return reducer;
			}

			throw new InvalidOperationException($"No reducer for action {actionType.Name} is registered");
		}
	}
}
