﻿using Durable.Common.Actions;
using System;

namespace Durable.Server.Reducers.Registry
{
	public interface IDurableReducerRegistryBuilder
	{
		IDurableReducerRegistryBuilder Register<TAction>(Func<IDurableReducer> reducerAccessor)
			where TAction : IDurableAction;
	}
}
