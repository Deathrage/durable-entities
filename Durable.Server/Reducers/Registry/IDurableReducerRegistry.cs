﻿using System;

namespace Durable.Server.Reducers.Registry
{
	public interface IDurableReducerRegistry: IDurableReducerRegistryBuilder
	{
		IDurableReducer ResolveReducer(Type actionType);
	}
}
