﻿using Durable.Common.Actions;
using Durable.Server.ChangeTrackers;
using System;
using System.Threading.Tasks;

namespace Durable.Server.Reducers.DefaultReducers
{
	public class GetStateReducer : IDurableReducer
	{
		public async ValueTask<object?> ReduceAsync(IDurableAction action, IDurableEntityChangeTracker changeTracker)
		{
			if (action is not GetStateAction getStateAction)
				throw new InvalidOperationException($"{nameof(GetStateReducer)} can only reduce {nameof(GetStateAction)}");

			object result = await changeTracker
				.GetStateAsync()
				.ConfigureAwait(false);

			return result;
		}
	}
}
