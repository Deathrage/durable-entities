﻿using Durable.Common.Actions;
using Durable.Server.ChangeTrackers;
using System;
using System.Threading.Tasks;

namespace Durable.Server.Reducers.DefaultReducers
{
	public class GetReducer : IDurableReducer
	{
		public async ValueTask<object?> ReduceAsync(IDurableAction action, IDurableEntityChangeTracker changeTracker)
		{
			if (action is not GetAction getAction)
				throw new InvalidOperationException($"{nameof(GetReducer)} can only reduce {nameof(GetAction)}");

			object? result = await changeTracker
				.GetMemberAsync(getAction.MemberName)
				.ConfigureAwait(false);

			return result;
		}
	}
}
