﻿using Durable.Common.Actions;
using Durable.Server.ChangeTrackers;
using System;
using System.Threading.Tasks;

namespace Durable.Server.Reducers.DefaultReducers
{
	public class SetReducer : IDurableReducer
	{
		public async ValueTask<object?> ReduceAsync(IDurableAction action, IDurableEntityChangeTracker changeTracker)
		{
			if (action is not SetAction setAction)
				throw new InvalidOperationException($"{nameof(SetReducer)} can only reduce {nameof(SetAction)}");

			object? result = await changeTracker
				.SetMemberAsync(setAction.MemberName, setAction.Value)
				.ConfigureAwait(false);

			return result;
		}
	}
}
