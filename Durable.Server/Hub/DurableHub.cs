﻿using Durable.Common.Actions;
using Durable.Common.Hub;
using Durable.Server.ChangeTrackers;
using Durable.Server.ChangeTrackers.Registry;
using Durable.Server.Reducers;
using Durable.Server.Reducers.Registry;
using System;
using System.Threading.Tasks;

namespace Durable.Server.Hub
{
	class DurableHub : IDurableHub
	{
		private readonly IDurableReducerRegistry _durableReducers;
		private readonly IChangeTrackerRegistry _durableEntityChangeTrackers;

		public DurableHub(IDurableReducerRegistry durableReducers, IChangeTrackerRegistry durableEntityChangeTrackers)
		{
			_durableReducers = durableReducers ?? throw new ArgumentNullException(nameof(durableReducers));
			_durableEntityChangeTrackers = durableEntityChangeTrackers ?? throw new ArgumentNullException(nameof(durableEntityChangeTrackers));
		}

		public async ValueTask<object?> DispatchAsync(IDurableAction action)
		{
			IDurableReducer reducer = _durableReducers.ResolveReducer(action.GetType());

			IDurableEntityChangeTracker changeTracker = await _durableEntityChangeTrackers
				.GetOrCreateAsync(action.EntityName, action.EntityKey)
				.ConfigureAwait(false);

			object? result = await reducer
				.ReduceAsync(action, changeTracker)
				.ConfigureAwait(false);

			return result;
		}
	}
}
