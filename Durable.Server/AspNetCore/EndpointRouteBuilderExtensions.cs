﻿using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Durable.Common.Http;
using System;
using Durable.Server.Actions;
using System.Text.Json;
using Durable.Common.Actions;
using Durable.Common.Hub;

namespace Durable.Server.AspNetCore
{
	public static class EndpointRouteBuilderExtensions
	{
		public static IEndpointConventionBuilder MapLocalDurableClient(this IEndpointRouteBuilder endpoints)
		=> endpoints.MapLocalDurableClient("/durable");

		public static IEndpointConventionBuilder MapLocalDurableClient(this IEndpointRouteBuilder endpoints, string path)
			=> endpoints.MapPost(path, async ctx =>
				{
					IDurableHub hub = ctx.RequestServices.GetRequiredService<IDurableHub>();
					IDurableActionRegistry actions = ctx.RequestServices.GetRequiredService<IDurableActionRegistry>();

					DurableHttpRequest? request = await ctx.Request
						.ReadFromJsonAsync<DurableHttpRequest>()
						.ConfigureAwait(false);

					if (request == null)
						throw new NullReferenceException(nameof(request));

					Type actionType = actions.ResolveActionType(request.ActionType);

					object? action = JsonSerializer.Deserialize(request.Action, actionType);

					if (action == null)
						throw new NullReferenceException(nameof(request));

					if (action is not IDurableAction durableAction)
						throw new InvalidOperationException($"Deserialized type is not {nameof(IDurableAction)}");

					DurableHttpResponse? response = null;
					if (request.ExpectsResult)
					{
						object? result = await hub
							.DispatchAsync(durableAction)
							.ConfigureAwait(false);

						response = new DurableHttpResponse(true, JsonSerializer.Serialize(result));

					}
					else
					{
						await hub
							.DispatchAsync(durableAction)
							.ConfigureAwait(false);

						response = new DurableHttpResponse(false, null);
					}

					ctx.Response.StatusCode = StatusCodes.Status200OK;
					await ctx.Response
						.WriteAsJsonAsync(response)
						.ConfigureAwait(false);
				});
	}
}
