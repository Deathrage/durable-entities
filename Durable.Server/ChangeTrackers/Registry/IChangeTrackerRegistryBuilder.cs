﻿namespace Durable.Server.ChangeTrackers.Registry
{
	public interface IChangeTrackerRegistryBuilder
	{
		CreateChangeTrackerAsync DefaultFactory { get; set; }

		IChangeTrackerRegistryBuilder RegisterEntityChangeTrackerFactory(string entityName, CreateChangeTrackerAsync factory);
	}
}
