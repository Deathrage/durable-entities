﻿using System.Threading.Tasks;

namespace Durable.Server.ChangeTrackers.Registry
{
	public delegate ValueTask<IDurableEntityChangeTracker> CreateChangeTrackerAsync(string entityName, string entityKey);
}
