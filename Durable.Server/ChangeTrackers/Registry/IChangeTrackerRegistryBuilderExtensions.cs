﻿using Durable.Server.ChangeTrackers.DefaultTrackers;
using System.Threading.Tasks;

namespace Durable.Server.ChangeTrackers.Registry
{
	public static class IChangeTrackerRegistryBuilderExtensions
	{
		private static readonly CreateChangeTrackerAsync _dictionaryChangeTrackerFactory = (entityName, entityKey) =>
		{
			IDurableEntityChangeTracker changeTracker = new InMemoryDictionaryChangeTracker(entityName, entityKey);

			return new ValueTask<IDurableEntityChangeTracker>(changeTracker);
		};

		public static IChangeTrackerRegistryBuilder UseDictionaryChangeTrackerAsDefault(this IChangeTrackerRegistryBuilder builder)
		{
			builder.DefaultFactory = _dictionaryChangeTrackerFactory;
			return builder;
		}
	}
}
