﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Durable.Server.ChangeTrackers.Registry
{
	public class DefaultChangeTrackerRegistry : IChangeTrackerRegistry
	{
		private readonly Dictionary<string, CreateChangeTrackerAsync> _knownTrackerFactories
			= new Dictionary<string, CreateChangeTrackerAsync>();

		private readonly Dictionary<(string entityName, string entityKey), IDurableEntityChangeTracker> _activeTrackers
			= new Dictionary<(string entityName, string entityKey), IDurableEntityChangeTracker>();

		private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

		private CreateChangeTrackerAsync? _defaultFactory;

		public CreateChangeTrackerAsync DefaultFactory
		{
			get => _defaultFactory ?? throw new NullReferenceException("Default Change Trakcer factory was not set");
			set => _defaultFactory = value;
		}

		public IChangeTrackerRegistryBuilder RegisterEntityChangeTrackerFactory(string entityName, CreateChangeTrackerAsync factory)
		{
			_knownTrackerFactories[entityName] = factory;

			return this;
		}

		public async ValueTask<IDurableEntityChangeTracker> GetOrCreateAsync(string entityName, string entityKey)
		{
			(string entityName, string entityKey) key = (entityName, entityKey);

			if (_activeTrackers.TryGetValue(key, out IDurableEntityChangeTracker? changeTrakcer) && changeTrakcer is not null)
				return changeTrakcer;

			await _semaphore.WaitAsync();

			if (_knownTrackerFactories.TryGetValue(key.entityName, out CreateChangeTrackerAsync? trackerFactory) && trackerFactory is not null)
				changeTrakcer = await trackerFactory(key.entityName, key.entityKey).ConfigureAwait(false);
			else
				changeTrakcer = await DefaultFactory(key.entityName, key.entityKey).ConfigureAwait(false);

			if (changeTrakcer is null)
				throw new InvalidOperationException("Change Tracker cannot be null");

			_activeTrackers[key] = changeTrakcer;

			_semaphore.Release();

			return changeTrakcer;
		}

		public void Remove(string entityName, string entityKey)
			=> _activeTrackers.Remove((entityName, entityKey));
	}
}
