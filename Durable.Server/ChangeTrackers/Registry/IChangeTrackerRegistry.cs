﻿using System.Threading.Tasks;

namespace Durable.Server.ChangeTrackers.Registry
{

	public interface IChangeTrackerRegistry: IChangeTrackerRegistryBuilder
	{
		ValueTask<IDurableEntityChangeTracker> GetOrCreateAsync(string entityName, string entityKey);

		void Remove(string entityName, string entityKey);
	}
}
