﻿using System.Threading.Tasks;

namespace Durable.Server.ChangeTrackers
{
	public interface IDurableEntityChangeTracker
	{
		string EntityName { get; }

		string EntityKey { get; }

		ValueTask<object> GetStateAsync();

		ValueTask<object?> GetMemberAsync(string name);

		ValueTask<object?> SetMemberAsync(string name, object? value);
	}
}
