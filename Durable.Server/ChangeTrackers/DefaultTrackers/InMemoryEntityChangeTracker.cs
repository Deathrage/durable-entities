﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Durable.Server.ChangeTrackers.DefaultTrackers
{
	public class InMemoryEntityChangeTracker : IDurableEntityChangeTracker
	{
		private IReadOnlyDictionary<string, FieldOrPropertyMember> _members;

		private readonly object _state;

		public string EntityName { get; }

		public string EntityKey { get; }

		public InMemoryEntityChangeTracker(string entityName, string entityKey, object initialState)
		{
			EntityName = entityName ?? throw new ArgumentNullException(nameof(entityName));
			EntityKey = entityKey ?? throw new ArgumentNullException(nameof(entityKey));

			_state = initialState ?? throw new ArgumentNullException(nameof(initialState));

			IEnumerable<FieldOrPropertyMember> fromProperties = _state.GetType().GetProperties().Select(propertyInfo => new FieldOrPropertyMember(propertyInfo));
			IEnumerable<FieldOrPropertyMember> fromFields = _state.GetType().GetFields().Select(fieldInfo => new FieldOrPropertyMember(fieldInfo));

			_members = fromProperties.Concat(fromFields).ToDictionary(item => item.Name, item => item);
		}

		public ValueTask<object?> GetMemberAsync(string name)
		{
			if (_members.TryGetValue(name, out FieldOrPropertyMember member))
			{
				object? value = member.GetValue(_state);
				return new ValueTask<object?>(value);
			}

			throw new MissingMemberException($"Type {_state.GetType().Name} has neither field nor property called {name}");
		}

		public ValueTask<object?> SetMemberAsync(string name, object? value)
		{
			if (_members.TryGetValue(name, out FieldOrPropertyMember member))
			{
				member.SetValue(_state, value);
				return new ValueTask<object?>(value);
			}

			throw new MissingMemberException($"Type {_state.GetType().Name} has neither field nor property called {name}");
		}

		public ValueTask<object> GetStateAsync()
			=> new ValueTask<object>(_state);
	}
}
