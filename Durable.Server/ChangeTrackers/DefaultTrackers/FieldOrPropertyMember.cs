﻿using System.Reflection;

namespace Durable.Server.ChangeTrackers.DefaultTrackers
{
	struct FieldOrPropertyMember
	{
		enum FieldOrPropertyMemberType
		{
			Field,
			Property
		}

		private readonly PropertyInfo? _propertyInfo;
		private readonly FieldInfo? _fieldInfo;
		private readonly FieldOrPropertyMemberType _type;

		public string Name { get; }

		public FieldOrPropertyMember(PropertyInfo propertyInfo)
		{
			_propertyInfo = propertyInfo;
			_fieldInfo = null;
			_type = FieldOrPropertyMemberType.Property;
			Name = propertyInfo.Name;
		}
		public FieldOrPropertyMember(FieldInfo fieldInfo)
		{
			_propertyInfo = null;
			_fieldInfo = fieldInfo;
			_type = FieldOrPropertyMemberType.Field;
			Name = fieldInfo.Name;
		}

		public object? GetValue(object obj)
		{
			if (_type == FieldOrPropertyMemberType.Field)
				return _fieldInfo!.GetValue(obj);
			else
				return _propertyInfo!.GetValue(obj);
		}

		public void SetValue(object obj, object? value)
		{
			if (_type == FieldOrPropertyMemberType.Field)
				_fieldInfo!.SetValue(obj, value);
			else
				_propertyInfo!.SetValue(obj, value);
		}
	}
}
