﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Durable.Server.ChangeTrackers.DefaultTrackers
{
	class InMemoryDictionaryChangeTracker : IDurableEntityChangeTracker
	{
		private readonly ConcurrentDictionary<string, object?> _state;

		public string EntityName { get; }

		public string EntityKey { get; }

		public InMemoryDictionaryChangeTracker(string entityName, string entityKey, IEnumerable<KeyValuePair<string, object?>>? initialState = null)
		{
			EntityName = entityName ?? throw new ArgumentNullException(nameof(entityName));
			EntityKey = entityKey ?? throw new ArgumentNullException(nameof(entityKey));

			_state = new ConcurrentDictionary<string, object?>(initialState ?? Enumerable.Empty<KeyValuePair<string, object?>>());
		}

		public ValueTask<object?> GetMemberAsync(string name)
		{
			object? result = _state.GetValueOrDefault(name);

			return new ValueTask<object?>(result);
		}

		public ValueTask<object> GetStateAsync()
			=> throw new NotImplementedException($"Total state cannot be read from Dictionary based change tracker");

		public ValueTask<object?> SetMemberAsync(string name, object? value)
		{
			_state[name] = value;

			return new ValueTask<object?>(value);
		}
	}
}
