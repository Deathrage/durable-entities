﻿using Durable.Common.Actions;
using System;
using System.Collections.Concurrent;

namespace Durable.Server.Actions
{
	public class DurableActionRegistry : IDurableActionRegistry
	{
		private readonly ConcurrentDictionary<string, Type> _actionTypes = new ConcurrentDictionary<string, Type>(); 

		public IDurableActionRegistryBuilder Register<TAction>() where TAction : IDurableAction
		{
			string actionType = typeof(TAction).Name;
			_actionTypes.AddOrUpdate(actionType, _ => typeof(TAction), (_, _) => typeof(TAction));

			return this;
		}

		public Type ResolveActionType(string actionType)
		{
			if (_actionTypes.TryGetValue(actionType, out Type? type) && type is not null)
				return type;

			throw new InvalidOperationException($"No action of type {actionType} is registered");
		}
	}
}
