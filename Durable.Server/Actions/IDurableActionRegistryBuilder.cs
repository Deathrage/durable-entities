﻿using Durable.Common.Actions;

namespace Durable.Server.Actions
{
	public interface IDurableActionRegistryBuilder
	{
		IDurableActionRegistryBuilder Register<TAction>() where TAction : IDurableAction;
	}
}
