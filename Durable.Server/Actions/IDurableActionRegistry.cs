﻿using System;

namespace Durable.Server.Actions
{
	public interface IDurableActionRegistry: IDurableActionRegistryBuilder
	{
		Type ResolveActionType(string actionType);
	}
}
