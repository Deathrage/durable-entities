﻿using Durable.Common.Actions;
using Durable.Common.Hub;
using Durable.Server.Actions;
using Durable.Server.ChangeTrackers;
using Durable.Server.ChangeTrackers.Registry;
using Durable.Server.Hub;
using Durable.Server.Reducers;
using Durable.Server.Reducers.DefaultReducers;
using Durable.Server.Reducers.Registry;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Durable.Server
{
	public delegate ValueTask<IDurableEntityChangeTracker> ChangeTrackerFactory(IServiceProvider provider, string entityName, string entityKey);

	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddDurableEntities(this IServiceCollection services, ChangeTrackerFactory changeTrackerFactory)
			=> services
				.AddDurableHub()
				.AddDurableActions()
				.AddDurableReducers()
				.AddDurableChangeTrackers();

		#region Hub

		public static IServiceCollection AddDurableHub(this IServiceCollection services)
		{
			if (services == null)
				throw new ArgumentNullException(nameof(services));

			return services.AddTransient<IDurableHub, DurableHub>();
		}

		#endregion

		#region Actions

		public static IServiceCollection AddDurableActions(this IServiceCollection services)
			=> services.AddDurableActions(actions => { });

		public static IServiceCollection AddDurableActions(this IServiceCollection services, Action<IDurableActionRegistryBuilder> config)
		{
			return services.AddSingleton<IDurableActionRegistry>(provider =>
			{
				DurableActionRegistry registry = new DurableActionRegistry();

				registry
					.Register<GetAction>()
					.Register<GetStateAction>()
					.Register<SetAction>();

				config(registry);

				return registry;
			});
		}

		#endregion

		#region Reducers

		public static IServiceCollection AddDurableReducers(this IServiceCollection services)
			=> services.AddDurableReducers((provider, reducers) => { });

		public static IServiceCollection AddDurableReducers(this IServiceCollection services, Action<IDurableReducerRegistryBuilder, IServiceProvider> config)
		{
			services
				.AddTransient<GetReducer>()
				.AddTransient<GetStateReducer>()
				.AddTransient<SetReducer>();

			return services.AddSingleton<IDurableReducerRegistry>(provider =>
			{
				DurableReducerRegistry registry = new DurableReducerRegistry();

				registry
					.Register<GetAction>(() => provider.GetRequiredService<GetReducer>())
					.Register<GetStateAction>(() => provider.GetRequiredService<GetStateReducer>())
					.Register<SetAction>(() => provider.GetRequiredService<SetReducer>());

				config(registry, provider);

				return registry;
			});
		}

		#endregion

		#region ChangeTracker

		public static IServiceCollection AddDurableChangeTrackers(this IServiceCollection services)
			=> services.AddDurableChangeTrackers((builder, _) => builder.UseDictionaryChangeTrackerAsDefault());

		public static IServiceCollection AddDurableChangeTrackers(this IServiceCollection services, Action<IChangeTrackerRegistryBuilder, IServiceProvider> configure)
			=> services.AddSingleton<IChangeTrackerRegistry>(provider =>
			{
				DefaultChangeTrackerRegistry registry = new DefaultChangeTrackerRegistry();

				configure(registry, provider);

				return registry;
			});

		#endregion
	}
}
