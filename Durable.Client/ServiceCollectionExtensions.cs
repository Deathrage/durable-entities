﻿using Durable.Client;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;

namespace Durable.Server
{
	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddNewtorkDurableClient(this IServiceCollection services, string hubPath)
		{
			if (services == null)
				throw new ArgumentNullException(nameof(services));

			return services.AddSingleton<IDurableClient>(provider =>
			{
				NewtorkDurableClient client = new NewtorkDurableClient(hubPath, provider.GetService<HttpClient>() ?? new HttpClient());
				return client;
			});
		}

		public static IServiceCollection AddLocalDurableClient(this IServiceCollection services)
		{
			if (services == null)
				throw new ArgumentNullException(nameof(services));

			return services.AddSingleton<IDurableClient, LocalDurableClient>();
		}
	}
}
