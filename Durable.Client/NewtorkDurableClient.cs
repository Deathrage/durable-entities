﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using Durable.Client.Entities;
using Durable.Common.Actions;
using Durable.Common.Http;

namespace Durable.Client
{
	class NewtorkDurableClient: IDurableClient
	{
		private readonly string _hubPath;

		private readonly HttpClient _httpClient;

		private  async ValueTask<DurableHttpResponse> PostAsync(IDurableAction action, bool expectesResult)
		{
			DurableHttpRequest request = new DurableHttpRequest(action.GetType().Name, JsonSerializer.Serialize(action, action.GetType()), expectesResult);

			HttpResponseMessage responseMessage = await _httpClient
				.PostAsJsonAsync(_hubPath, request)
				.ConfigureAwait(false);

			if (responseMessage == null)
				throw new NullReferenceException(nameof(responseMessage));

			responseMessage.EnsureSuccessStatusCode();

			DurableHttpResponse? response = await responseMessage.Content
				.ReadFromJsonAsync<DurableHttpResponse>()
				.ConfigureAwait(false);

			if (response == null)
				throw new NullReferenceException(nameof(response));

			return response;
		}

		public NewtorkDurableClient(string hubPath, HttpClient httpClient)
		{
			_hubPath = hubPath ?? throw new ArgumentNullException(nameof(hubPath));
			_httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
		}

		public async ValueTask<TReturn?> DispatchAsync<TReturn>(IDurableAction action)
		{
			DurableHttpResponse response = await PostAsync(action, true)
				.ConfigureAwait(false);

			if (!response.HasResult || response.Result is null)
				throw new InvalidOperationException("Operation has no result, but result is expected");

			TReturn? result = JsonSerializer.Deserialize<TReturn>(response.Result);

			return result;
		}

		public async ValueTask DispatchAsync(IDurableAction action)
{
			DurableHttpResponse response = await PostAsync(action, false)
				.ConfigureAwait(false);

			if (response.HasResult)
				throw new InvalidOperationException("Operation has result, but no result is expected");
		}

		public IDurableEntity<TEntity> MakeDurable<TEntity>(string entityKey)
		{
			string entityName = typeof(TEntity).Name;

			IDurableEntity<TEntity> entity = new DurableEntity<TEntity>(entityName, entityKey, this);

			return entity;
		}
	}
}
