﻿using Durable.Client.Entities;
using Durable.Common.Actions;
using System.Threading.Tasks;

namespace Durable.Client
{
	public interface IDurableClient
	{
		ValueTask<TReturn?> DispatchAsync<TReturn>(IDurableAction action);
		ValueTask DispatchAsync(IDurableAction action);

		IDurableEntity<TEntity> MakeDurable<TEntity>(string entityKey);
	}
}
