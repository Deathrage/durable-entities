﻿using System;
using System.Threading.Tasks;
using Durable.Client.Entities;
using Durable.Common.Actions;
using Durable.Common.Hub;

namespace Durable.Client
{
	public class LocalDurableClient : IDurableClient
	{
		private readonly IDurableHub _durableHub;

		public LocalDurableClient(IDurableHub durableHub)
		{
			_durableHub = durableHub ?? throw new ArgumentNullException(nameof(durableHub));
		}

		public async ValueTask<TReturn?> DispatchAsync<TReturn>(IDurableAction action)
		{
			object? result = await _durableHub
				.DispatchAsync(action)
				.ConfigureAwait(false);

			return (TReturn?)result;
		}

		public async ValueTask DispatchAsync(IDurableAction action)
		{
			await _durableHub
				.DispatchAsync(action)
				.ConfigureAwait(false);
		}

		public IDurableEntity<TEntity> MakeDurable<TEntity>(string entityKey)
		{
			string entityName = typeof(TEntity).Name;

			IDurableEntity<TEntity> entity = new DurableEntity<TEntity>(entityName, entityKey, this);

			return entity;
		}
	}
}
