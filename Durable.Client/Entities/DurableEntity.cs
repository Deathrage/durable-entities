﻿using System;

namespace Durable.Client.Entities
{
	public class DurableEntity<TEntity> : IDurableEntity<TEntity>
	{
		public string EntityName { get; }

		public string EntityKey { get; }

		public IDurableClient DurableClient { get; }

		public DurableEntity(string entityName, string entityKey, IDurableClient durableClient)
		{
			EntityName = entityName ?? throw new ArgumentNullException(nameof(entityName));
			EntityKey = entityKey ?? throw new ArgumentNullException(nameof(entityKey));
			DurableClient = durableClient ?? throw new ArgumentNullException(nameof(durableClient));
		}
	}
}
