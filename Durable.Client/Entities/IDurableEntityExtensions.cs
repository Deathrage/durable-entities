﻿using Durable.Common.Actions;
using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Durable.Client.Entities
{
	public static class IDurableEntityExtensions
	{
        public static async ValueTask<TEntity> GetStateAsync<TEntity>(this IDurableEntity<TEntity> entity)
        {
            GetStateAction getAction = new GetStateAction(entity.EntityName, entity.EntityKey);

            TEntity? result = await entity.DurableClient
                .DispatchAsync<TEntity>(getAction)
                .ConfigureAwait(false);

            if (result is null)
                throw new InvalidOperationException("Returned durable state cannot be null");

            return result;
        }

        public static ValueTask<TValue?> GetAsync<TEntity,TValue>(this IDurableEntity<TEntity> entity, Expression<Func<TEntity, TValue>> selector)
		{
            string memberName = GetMemberName(selector);

            GetAction getAction = new GetAction(entity.EntityName, entity.EntityKey, memberName);

            return entity.DurableClient.DispatchAsync<TValue>(getAction);
        }

		public static ValueTask SetAsync<TEntity, TValue>(this IDurableEntity<TEntity> entity, Expression<Func<TEntity, TValue>> selector, TValue value)
        {
            string memberName = GetMemberName(selector);

            SetAction setAction = new SetAction(entity.EntityName, entity.EntityKey, memberName, value);

            return entity.DurableClient.DispatchAsync(setAction);
        }

        private static string GetMemberName<TSource, TProperty>(Expression<Func<TSource, TProperty>> propertyLambda)
        {
            Type type = typeof(TSource);

            MemberExpression? member = propertyLambda.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    propertyLambda.ToString()));

            MemberInfo memberInfo = member.Member;

            if (memberInfo.ReflectedType is null)
                throw new ArgumentException($"Expression '{propertyLambda}' refers to a member that is not from any type.");

            if (type != memberInfo.ReflectedType && !type.IsSubclassOf(memberInfo.ReflectedType))
                throw new ArgumentException($"Expression '{propertyLambda}' refers to a property that is not from type {type}.");

            return memberInfo.Name;
        }
    }
}
