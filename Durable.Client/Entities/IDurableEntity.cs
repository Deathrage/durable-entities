﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Durable.Client.Entities
{
	public interface IDurableEntity<TEntity>
	{
		string EntityName { get; }

		string EntityKey { get; }

		IDurableClient DurableClient { get; }
	}
}
