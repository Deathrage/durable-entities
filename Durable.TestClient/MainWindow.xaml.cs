﻿using Durable.Client;
using Durable.Client.Entities;
using Durable.TestCommon.Model;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Windows;

namespace Durable.TestClient
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private Person? _currentPerson;
		private Person? CurrentPerson
		{
			get => _currentPerson;
			set
			{
				_currentPerson = value;
				string json = JsonConvert.SerializeObject(value, Formatting.Indented);
				txtPerson.Text = json;
				btnAddBalance.IsEnabled = true;
			}
		}

		private readonly HttpClient _httpClient;

		private readonly IDurableClient _durableClient;

		public MainWindow(HttpClient httpClient, IDurableClient durableClient)
		{
			InitializeComponent();
			_httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
			_durableClient = durableClient ?? throw new ArgumentNullException(nameof(durableClient));
		}

		private async void btnLoad_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				HttpResponseMessage res = await _httpClient.GetAsync("http://localhost:5000/person/" + txtId.Text);

				if (res.StatusCode == System.Net.HttpStatusCode.OK)
				{
					string resBody = await res.Content.ReadAsStringAsync();
					CurrentPerson = JsonConvert.DeserializeObject<Person>(resBody);
					return;
				}

				if (res.StatusCode == System.Net.HttpStatusCode.NotFound)
				{
					txtPerson.Text = "No Person for Id " + txtId.Text;
					return;
				}
			}
			catch
			{
			}

			txtPerson.Text = "Something failed";
		}

		private async void btnLoadDurable_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				IDurableEntity<Person> durablePerson = _durableClient.MakeDurable<Person>(txtId.Text);

				Person person = await durablePerson.GetStateAsync();

				if (person == null)
				{
					txtPerson.Text = "No Person for Id " + txtId.Text;
					return;
				}

				CurrentPerson = person;
				return;
			}
			catch
			{
			}

			txtPerson.Text = "Something failed";
		}

		private async void btnAddBalance_Click(object sender, RoutedEventArgs e)
		{
			if (CurrentPerson is null || txtBalance.Text is null || !decimal.TryParse(txtBalance.Text, out decimal balancetoAdd))
				return;

			IDurableEntity<Person> durablePerson = _durableClient.MakeDurable<Person>(CurrentPerson.Id.ToString());

			decimal currentbalance = await durablePerson.GetAsync(person => person.Balance);
			await durablePerson.SetAsync(person => person.Balance, currentbalance + balancetoAdd);

			Person person = await durablePerson.GetStateAsync();
			CurrentPerson = person;
		}
	}
}
