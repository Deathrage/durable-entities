﻿using Durable.Server;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows;

namespace Durable.TestClient
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private readonly IServiceProvider _serviceProvider;

		public App()
		{
			IServiceCollection serviceCollection = new ServiceCollection();
			ConfigureServices(serviceCollection);
			_serviceProvider = serviceCollection.BuildServiceProvider();
		}

		private void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<MainWindow>();
			services.AddHttpClient();
			services.AddNewtorkDurableClient("http://localhost:5000/durable");
		}

		private void OnStartup(object sender, StartupEventArgs e)
		{
			MainWindow mainWindow = _serviceProvider.GetRequiredService<MainWindow>();
			mainWindow.Show();
		}
	}
}
