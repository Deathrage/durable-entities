﻿using Durable.TestCommon.Model;
using System.Collections.Generic;
using System.Linq;

namespace Durable.TestServer.Repository
{
	public class PersonRepository
	{
		private static readonly Dictionary<int, Person> _people = new Dictionary<int, Person>()
		{
			{1, new Person() { Id = 1, Name = "John Doe", Balance = 500 } },
			{2, new Person() { Id = 2, Name = "Jane Doe", Balance = 250 } },
		};

		public Person GetPerson(int id)
		{
			if (_people.TryGetValue(id, out Person person)) 
				return person;
			return null;
		}

		public Person SavePerson(Person person)
		{
			if (person.Id > 0)
			{
				_people[person.Id] = person;
				return person;
			}

			int newKey = _people.Keys.Max() + 1;
			person.Id = newKey;
			_people[newKey] = person;

			return person;
		}
	}
}
