﻿using Durable.TestCommon.Model;
using Durable.TestServer.Repository;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Durable.TestServer.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class PersonController : ControllerBase
	{
		private readonly PersonRepository _personRepository;

		public PersonController(PersonRepository personRepository)
		{
			_personRepository = personRepository ?? throw new ArgumentNullException(nameof(personRepository));
		}

		[HttpGet("{id:int}")]
		public IActionResult Get(int id)
		{
			Person person = _personRepository.GetPerson(id);
			if (person == null)
				return NotFound();
			return Ok(person);
		}
	}
}
