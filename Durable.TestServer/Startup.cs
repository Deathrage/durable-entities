using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Durable.Server;
using Durable.Server.AspNetCore;
using Durable.Server.ChangeTrackers;
using Durable.TestCommon.Model;
using System;
using Durable.TestServer.Repository;
using Durable.Server.ChangeTrackers.Registry;
using Durable.Server.ChangeTrackers.DefaultTrackers;

namespace Durable.TestServer
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services
				.AddDurableHub()
				.AddDurableActions()
				.AddDurableReducers()
				.AddDurableChangeTrackers((builder, provider) =>
				{
					builder
						.RegisterEntityChangeTrackerFactory(nameof(Person), async (entityName, entityKey) =>
						{
							if (int.TryParse(entityKey, out int id))
							{
								PersonRepository personRepository = provider.GetRequiredService<PersonRepository>();
								Person person = personRepository.GetPerson(id);
								return new InMemoryEntityChangeTracker(entityName, entityKey, person);
							}

							throw new InvalidOperationException($"EntityKey {entityKey} for {entityName} is not null or valid int");
						})
						.UseDictionaryChangeTrackerAsDefault();
				})
				.AddTransient<PersonRepository>()
				.AddControllers();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app
				.UseRouting()
				.UseAuthorization()
				.UseEndpoints(endpoints =>
				{
					endpoints.MapControllers();
					endpoints.MapLocalDurableClient();
				});
		}
	}
}
