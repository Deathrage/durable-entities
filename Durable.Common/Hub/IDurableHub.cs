﻿using Durable.Common.Actions;
using System.Threading.Tasks;

namespace Durable.Common.Hub
{
	public interface IDurableHub
	{
		ValueTask<object?> DispatchAsync(IDurableAction action);
	}
}
