﻿namespace Durable.Common.Actions
{
	public interface IDurableAction
	{
		public string EntityName { get; }

		public string EntityKey { get; }
	}
}
