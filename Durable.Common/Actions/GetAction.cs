﻿using System;

namespace Durable.Common.Actions
{
	public sealed class GetAction : IDurableAction
	{
		public string EntityName { get; }

		public string EntityKey { get; }

		public string MemberName { get; }

		public GetAction(string entityName, string entityKey, string memberName)
		{
			EntityName = entityName ?? throw new ArgumentNullException(nameof(entityName));
			EntityKey = entityKey ?? throw new ArgumentNullException(nameof(entityKey));
			MemberName = memberName ?? throw new ArgumentNullException(nameof(memberName));
		}
	}
}
