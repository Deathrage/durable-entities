﻿using System;

namespace Durable.Common.Actions
{
	public class GetStateAction: IDurableAction
	{
		public string EntityName { get; }

		public string EntityKey { get; }

		public GetStateAction(string entityName, string entityKey)
		{
			EntityName = entityName ?? throw new ArgumentNullException(nameof(entityName));
			EntityKey = entityKey ?? throw new ArgumentNullException(nameof(entityKey));
		}
	}
}
