﻿using System;

namespace Durable.Common.Actions
{
	public class SetAction: IDurableAction
	{
		public string EntityName { get; }

		public string EntityKey { get; }

		public string MemberName { get; }

		public object? Value { get; }

		public SetAction(string entityName, string entityKey, string memberName, object? value)
		{
			EntityName = entityName ?? throw new ArgumentNullException(nameof(entityName));
			EntityKey = entityKey ?? throw new ArgumentNullException(nameof(entityKey));
			MemberName = memberName ?? throw new ArgumentNullException(nameof(memberName));
			Value = value;
		}
	}
}
