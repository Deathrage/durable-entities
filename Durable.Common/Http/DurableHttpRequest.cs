﻿using System;

namespace Durable.Common.Http
{
	public class DurableHttpRequest
	{
		public string ActionType { get; }
		
		public string Action { get; }

		public bool ExpectsResult { get; }

		public DurableHttpRequest(string actionType, string action, bool expectsResult)
		{
			ActionType = actionType ?? throw new ArgumentNullException(nameof(actionType));
			Action = action ?? throw new ArgumentNullException(nameof(action));
			ExpectsResult = expectsResult;
		}
	}
}
