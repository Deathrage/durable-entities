﻿using System;

namespace Durable.Common.Http
{
	public class DurableHttpResponse
	{
		public bool HasResult { get; }

		public string? Result { get; }

		public DurableHttpResponse(bool hasResult, string? result)
		{
			HasResult = hasResult;
			Result = result ?? throw new ArgumentNullException(nameof(result));
		}
	}
}
